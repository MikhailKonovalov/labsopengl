#include <GL/glut.h>


double rotate_x = 0;
double rotate_y = 0;

float delta = 0.0f;

float speed_of_cube_rotation = 0.2f;

enum cube_rotation{
	non = 0,
	cube_x_up_rotation,
	cube_x_down_rotation,
	cube_y_left_rotation,
	cube_y_right_rotation
};

cube_rotation cube_rot = non;

void drawCube()
{
   
    //FRONT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 0.0); // �������
    glVertex3f(0.3, -0.3, -0.3 - delta);
    glVertex3f(0.3, 0.3, -0.3 - delta);
    glVertex3f(-0.3, 0.3, -0.3 - delta);
    glVertex3f(-0.3, -0.3, -0.3 - delta);
    glEnd();

    //BACK
    glBegin(GL_POLYGON);
    glColor3f(0.0, 0.0, 1.0); // �����
    glVertex3f(0.3, -0.3, 0.3 + delta);
    glVertex3f(0.3, 0.3, 0.3 + delta);
    glVertex3f(-0.3, 0.3, 0.3 + delta);
    glVertex3f(-0.3, -0.3, 0.3 + delta);
    glEnd();

    //LEFT
    glBegin(GL_POLYGON);
    glColor3f(1.0, 1.0, 0.0); // ������
    glVertex3f(-0.3 - delta, -0.3, 0.3);
    glVertex3f(-0.3 - delta, 0.3, 0.3);
    glVertex3f(-0.3 - delta, 0.3, -0.3);
    glVertex3f(-0.3 - delta, -0.3, -0.3);
    glEnd();

    //RIGHT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 1.0); // ���������
    glVertex3f(0.3 + delta, -0.3, -0.3);
    glVertex3f(0.3 + delta, 0.3, -0.3);
    glVertex3f(0.3 + delta, 0.3, 0.3);
    glVertex3f(0.3 + delta, -0.3, 0.3);
    glEnd();

    //TOP
    glBegin(GL_POLYGON);
    glColor3f(1.0, 0.0, 0.0); // �������
    glVertex3f(0.3, 0.3 + delta, 0.3);
    glVertex3f(0.3, 0.3 + delta, -0.3);
    glVertex3f(-0.3, 0.3 + delta, -0.3);
    glVertex3f(-0.3, 0.3 + delta, 0.3);
    glEnd();

    //BOTTOM
    glBegin(GL_POLYGON);
    glColor3f(1.0, 0.0, 1.0); // ���������
    glVertex3f(0.3, -0.3 - delta, -0.3);
    glVertex3f(0.3, -0.3 - delta, 0.3);
    glVertex3f(-0.3, -0.3 - delta, 0.3);
    glVertex3f(-0.3, -0.3 - delta, -0.3);
    glEnd();

}

void movement_logic()
{
    switch(cube_rot)
    {
		case cube_y_right_rotation:
			rotate_y += speed_of_cube_rotation;
			break;
		case cube_y_left_rotation:
			rotate_y -= speed_of_cube_rotation;
			break;
		case cube_x_up_rotation:
			rotate_x += speed_of_cube_rotation;
			break;
		case cube_x_down_rotation:
			rotate_x -= speed_of_cube_rotation;
			break;
		
		default:
			break;
	}
    glutPostRedisplay();
}


void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glPushMatrix();
    glRotatef(rotate_x, 1.0, 0.0, 0.0);
    glRotatef(rotate_y, 0.0, 1.0, 0.0);
    drawCube();
    movement_logic();
    glPopMatrix();
    glutSwapBuffers();
    if(delta > 0.4f) delta = 0.4f;
    if(delta < 0.0f) delta = 0.0f;
}

void Reshape(int Width, int Height)
{
    if (Height == 0) Height = 1;
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (GLdouble)Width, 0.0, (GLdouble)Height);
    display();
    glutSwapBuffers();
}

void init()
{
    glClearColor(1.0, 1.0, 1.0, 1.0);
  //glMatrixMode(GL_MODELVIEW);
  //glLoadIdentity();
  //gluLookAt(5.0, -5.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

void specialKeys(int key, int x, int y) 
{
    switch (key)
    {
    default:
        break;

    case GLUT_KEY_RIGHT:
		if(cube_rot == non )cube_rot = cube_y_right_rotation;
		else cube_rot = non;
		break;

	case GLUT_KEY_LEFT:
		if(cube_rot == non ) cube_rot = cube_y_left_rotation;
		else cube_rot = non;
		break;

	case GLUT_KEY_UP:
		if(cube_rot == non ) cube_rot = cube_x_up_rotation;
		else cube_rot = non;	
		break;

	case GLUT_KEY_DOWN:
		if(cube_rot == non ) cube_rot = cube_x_down_rotation;
		else cube_rot = non;
		break;

    case GLUT_KEY_F3:
        delta += 0.01f;
        glutPostRedisplay();
        break;
    
    case GLUT_KEY_F4:
        delta -= 0.01f;
        glutPostRedisplay();
        break;
    }
}

void timer(int t)
{
    //if (autoRotate) rotate_y += dir;

    glutPostRedisplay();
    glutTimerFunc(10, timer, 1);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(400, 400);
    glutInitWindowSize(800, 600);
    glutCreateWindow("OpenGL Window");
    init();
    glutDisplayFunc(display);
    glutTimerFunc(10, timer, 1);
    //glutIdleFunc(display);
    glutReshapeFunc(Reshape);
    glutSpecialFunc(specialKeys);

    glutMainLoop();
    return 0;
}

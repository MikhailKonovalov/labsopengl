all:comp
comp:
	mkdir build
	g++ labs/lab1/lab1.cpp -o build/lab1 -lGL -lGLU -lglut
	g++ labs/lab2/lab2.cpp -o build/lab2 -lGL -lGLU -lglut
	g++ labs/lab3/lab3.cpp -o build/lab3 -lGL -lGLU -lglut
	g++ Cube/clear/lightcube.cpp -o build/commoncube -lGL -lGLU -lglut
	g++ Cube/common/commoncube.cpp -o build/clearcube -lGL -lGLU -lglut
clean:
	rm build/lab1
	rm build/lab2
	rm build/lab3
	rm build/commoncube
	rm build/clearcube
	rm -r build

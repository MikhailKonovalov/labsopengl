#include <iostream>
#include <math.h>
//#define GL_GLEXT_PROTOTYPES
#include <GL/glut.h>
#include <GL/gl.h>
//#define STB_IMAGE_IMPLEMENTATION
//#include <stb_image.h>

#define ScreenW 800
#define ScreenH 800


static float point1[3] = { 0.0f,0.0f,0.0f };
static float point2[3] = { 0.0f,0.0f,0.0f };
static float point3[3] = { 0.0f,0.0f,0.0f };
static float point4[3] = { 0.0f,0.0f,0.0f };

float pointA = 0.25f;
float pointX = 0.25f;
float pointY = 0.0f;
float pointZ = 0.0f;

double rotate_x = 0;
double rotate_y = 0;

//static int dir = 0;

float delta = 0.0f;
float angleOfRotation = 1.0f;
float dir = 0.5f;
float space = 0.5f;
float alpha = 0.0f;
float theta = 0.0f;

bool autoRotate = false;
bool light = true;
bool isAlpha = false;

bool cube_is_multicolor = true;
bool cube_is_multitexture = false; 
bool cube_is_textured = false;
bool cube_is_slized = false;
bool blend = false;

float sphereLightnessX = -0.5;
float sphereLightnessZ = 0.5;

float speed_of_cube_rotation = 0.2f;
float speed_of_sphere_rotation = 0.2f;

enum sphere_rotation{
	nope = 0,
	sphere_left_rotation,
	sphere_right_rotation
};

enum cube_rotation{
	non = 0,
	cube_x_up_rotation,
	cube_x_down_rotation,
	cube_y_left_rotation,
	cube_y_right_rotation
};


cube_rotation cube_rot = non;
sphere_rotation sphere_rot = nope;
//
//struct text_img
//{
//    unsigned char* data;
//    int sizeX, sizeY, n;
//};
//char text_name[6][11]{
//    "a.png",  "b.png" , "c.png", "d.png",   "e.png",  "f.png"
//};
//
//GLuint textures[6];
//int texsum = sizeof(textures) / sizeof(textures[0]);

GLfloat cubeArr[]
{
    // Top
    0.5, 0.5, 0.5,
    0.5, 0.5, -0.5,
    -0.5, 0.5, -0.5,
    -0.5, 0.5, 0.5,
    // Front
    0.5, -0.5, -0.5,
    0.5, 0.5, -0.5,
    -0.5, 0.5, -0.5,
    -0.5, -0.5, -0.5,
    // Top
    0.5, 0.5, 0.5,
    0.5, 0.5, -0.5,
    -0.5, 0.5, -0.5,
    -0.5, 0.5, 0.5,

    // Right
    0.5, -0.5, -0.5,
    0.5, 0.5, -0.5,
    0.5, 0.5, 0.5,
    0.5, -0.5, 0.5,
    
    // Left
    -0.5, -0.5, 0.5,
    -0.5, 0.5, -0.5,
    -0.5, 0.5, -0.5,
    -0.5, -0.5, 0.5,
    // Back
    0.5, -0.5, 0.5,
    0.5, 0.5, 0.5,
    -0.5, 0.5, 0.5,
    -0.5, -0.5, 0.5,
    
    
    // Bottom
    0.5, -0.5, -0.5,
    0.5, -0.5, 0.5,
    -0.5, -0.5, 0.5,
    -0.5, -0.5, -0.5

};
void drawCubeN()
{

    //FRONT
    glBegin(GL_POLYGON);
    glNormal3f(0, 0, 1);
    glColor3f(0.0, 1.0, 0.0); // �������
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();

    //BACK
    glBegin(GL_POLYGON);
    glColor3f(0.0, 0.0, 1.0); // �����
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glVertex3f(-0.5, -0.5, 0.5);
    glEnd();

    //RIGHT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 1.0); // ���������
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glEnd();

    //LEFT
    glBegin(GL_POLYGON); // ������
    glColor3f(1.0, 1.0, 0.0);
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();

    //TOP
    glBegin(GL_POLYGON);
    glColor3f(1.0, 0.0, 0.0); // �������
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glEnd();

    //BOTTOM
    glBegin(GL_POLYGON);
    glColor3f(1.0, 0.0, 1.0); // ���������
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();


}
void drawCubeS()
{
    //FRONT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 0.0); // �������
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, -0.5, -0.5);
    glNormal3f(0, 0, 1);
    glEnd();

    //BACK
    glBegin(GL_POLYGON);
    glColor3f(0.0, 0.0, 1.0); // �����
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glVertex3f(-0.5, -0.5, 0.5);
    glNormal3f(0, 0, -1);
    glEnd();

    //RIGHT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 1.0); // ���������
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glNormal3f(-1, 0, 0);
    glEnd();

    //LEFT
    glBegin(GL_POLYGON); // ������
    glColor3f(1.0, 1.0, 0.0);
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, -0.5, -0.5);
    glNormal3f(1, 0, 0);
    glEnd();

    //TOP
    glBegin(GL_POLYGON);
    glNormal3f(0, 1, 0);
    glColor3f(1.0, 0.0, 0.0); // �������
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glEnd();

    //BOTTOM
    glBegin(GL_POLYGON);
    glNormal3f(0, -1, 0);
    glColor3f(1.0, 0.0, 1.0); // ���������
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();
}
void drawCube()
{
   
    //FRONT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 0.0); // �������
    glNormal3f(0.0, 0.0, 1.0);
    glVertex3f(0.3, -0.3, -0.3 - delta);
    glVertex3f(0.3, 0.3, -0.3 - delta);
    glVertex3f(-0.3, 0.3, -0.3 - delta);
    glVertex3f(-0.3, -0.3, -0.3 - delta);
    //glNormal3f(0, 0, 1);
    glEnd();

    //BACK
    glBegin(GL_POLYGON);
    glColor3f(0.0, 0.0, 1.0); // �����
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(0.3, -0.3, 0.3 + delta);
    glVertex3f(0.3, 0.3, 0.3 + delta);
    glVertex3f(-0.3, 0.3, 0.3 + delta);
    glVertex3f(-0.3, -0.3, 0.3 + delta);
    //glNormal3f(0, 0, -1);
    glEnd();

    //LEFT
    glBegin(GL_POLYGON);
    glColor3f(1.0, 1.0, 0.0); // ������
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-0.3 - delta, -0.3, 0.3);
    glVertex3f(-0.3 - delta, 0.3, 0.3);
    glVertex3f(-0.3 - delta, 0.3, -0.3);
    glVertex3f(-0.3 - delta, -0.3, -0.3);
    //glNormal3f(1, 0, 0);
    glEnd();

    //RIGHT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 1.0); // ���������
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(0.3 + delta, -0.3, -0.3);
    glVertex3f(0.3 + delta, 0.3, -0.3);
    glVertex3f(0.3 + delta, 0.3, 0.3);
    glVertex3f(0.3 + delta, -0.3, 0.3);
    glEnd();

    //TOP
    glBegin(GL_POLYGON);
    glColor3f(1.0, 0.0, 0.0); // �������
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(0.3, 0.3 + delta, 0.3);
    glVertex3f(0.3, 0.3 + delta, -0.3);
    glVertex3f(-0.3, 0.3 + delta, -0.3);
    glVertex3f(-0.3, 0.3 + delta, 0.3);
    glEnd();

    //BOTTOM
    glBegin(GL_POLYGON);
    //glNormal3f(0, -1, 0);
    glColor3f(1.0, 0.0, 1.0); // ���������
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(0.3, -0.3 - delta, -0.3);
    glVertex3f(0.3, -0.3 - delta, 0.3);
    glVertex3f(-0.3, -0.3 - delta, 0.3);
    glVertex3f(-0.3, -0.3 - delta, -0.3);
    glEnd();

}

void drawTextureCube()
{

    //FRONT
    glBegin(GL_POLYGON);
    //glColor3f(0.0, 1.0, 0.0); // �������
    glTexCoord2f(0.0, 0.0);
    glVertex3f(0.3, -0.3, -0.3 - delta);
    glTexCoord2f(1.0, 0.0);
    glVertex3f(0.3, 0.3, -0.3 - delta);
    glTexCoord2f(1.0, 1.0);
    glVertex3f(-0.3, 0.3, -0.3 - delta);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-0.3, -0.3, -0.3 - delta);
    //glNormal3f(0, 0, 1);
    glEnd();

    //BACK
    glBegin(GL_POLYGON);
    glColor3f(0.0, 0.0, 1.0); // �����
    glVertex3f(0.3, -0.3, 0.3 + delta);
    glVertex3f(0.3, 0.3, 0.3 + delta);
    glVertex3f(-0.3, 0.3, 0.3 + delta);
    glVertex3f(-0.3, -0.3, 0.3 + delta);
    //glNormal3f(0, 0, -1);
    glEnd();

    //LEFT
    glBegin(GL_POLYGON);
    glColor3f(1.0, 1.0, 0.0); // ������
    glVertex3f(-0.3 - delta, -0.3, 0.3);
    glVertex3f(-0.3 - delta, 0.3, 0.3);
    glVertex3f(-0.3 - delta, 0.3, -0.3);
    glVertex3f(-0.3 - delta, -0.3, -0.3);
    //glNormal3f(1, 0, 0);
    glEnd();

    //RIGHT
    glBegin(GL_POLYGON);
    glColor3f(0.0, 1.0, 1.0); // ���������
    glVertex3f(0.3 + delta, -0.3, -0.3);
    glVertex3f(0.3 + delta, 0.3, -0.3);
    glVertex3f(0.3 + delta, 0.3, 0.3);
    glVertex3f(0.3 + delta, -0.3, 0.3);
    glNormal3f(-1, 0, 0);
    glEnd();

    //TOP
    glBegin(GL_POLYGON);
    //glNormal3f(0, 1, 0);
    glColor3f(1.0, 0.0, 0.0); // �������
    glVertex3f(0.3, 0.3 + delta, 0.3);
    glVertex3f(0.3, 0.3 + delta, -0.3);
    glVertex3f(-0.3, 0.3 + delta, -0.3);
    glVertex3f(-0.3, 0.3 + delta, 0.3);
    glEnd();

    //BOTTOM
    glBegin(GL_POLYGON);
    //glNormal3f(0, -1, 0);
    glColor3f(1.0, 0.0, 1.0); // ���������
    glVertex3f(0.3, -0.3 - delta, -0.3);
    glVertex3f(0.3, -0.3 - delta, 0.3);
    glVertex3f(-0.3, -0.3 - delta, 0.3);
    glVertex3f(-0.3, -0.3 - delta, -0.3);
    glEnd();

}

//void drawCubeT()
//{
//    //glBindTexture()
//    //FRONT
//    glBegin(GL_POLYGON);
//    glColor3f(0.0, 1.0, 0.0); // �������
//    glTexCoord3f(0.5, 0.5, 0.5);
//    glVertex3f(0.5, -0.5, -0.5);
//    glTexCoord3f(0.5, 0.5, - 0.5);
//    glVertex3f(0.5, 0.5, -0.5);
//    glTexCoord3f(-0.5, 0.5, -0.5);
//    glVertex3f(-0.5, 0.5, -0.5);
//    glTexCoord3f(-0.5, -0.5, -0.5);
//    glVertex3f(-0.5, -0.5, -0.5);
//    glEnd();
//
//    //BACK
//    glBegin(GL_POLYGON);
//    glColor3f(0.0, 0.0, 1.0); // �����
//    glVertex3f(0.5, -0.5, 0.5);
//    glTexCoord3f(0.5, 0.5, -0.5);
//    glVertex3f(0.5, 0.5, 0.5);
//    glVertex3f(-0.5, 0.5, 0.5);
//    glVertex3f(-0.5, -0.5, 0.5);
//    glEnd();
//
//    //RIGHT
//    glBegin(GL_POLYGON);
//    glColor3f(0.0, 1.0, 1.0); // ���������
//    glVertex3f(0.5, -0.5, -0.5);
//    glVertex3f(0.5, 0.5, -0.5);
//    glVertex3f(0.5, 0.5, 0.5);
//    glVertex3f(0.5, -0.5, 0.5);
//    glEnd();
//
//    //LEFT
//    glBegin(GL_POLYGON); // ������
//    glColor3f(1.0, 1.0, 0.0);
//    glVertex3f(-0.5, -0.5, 0.5);
//    glVertex3f(-0.5, 0.5, 0.5);
//    glVertex3f(-0.5, 0.5, -0.5);
//    glVertex3f(-0.5, -0.5, -0.5);
//    glEnd();
//
//    //TOP
//    glBegin(GL_POLYGON);
//    glColor3f(1.0, 0.0, 0.0); // �������
//    glVertex3f(0.5, 0.5, 0.5);
//    glVertex3f(0.5, 0.5, -0.5);
//    glVertex3f(-0.5, 0.5, -0.5);
//    glVertex3f(-0.5, 0.5, 0.5);
//    glEnd();
//
//    //BOTTOM
//    glBegin(GL_POLYGON);
//    glColor3f(1.0, 0.0, 1.0); // ���������
//    glVertex3f(0.5, -0.5, -0.5);
//    glVertex3f(0.5, -0.5, 0.5);
//    glVertex3f(-0.5, -0.5, 0.5);
//    glVertex3f(-0.5, -0.5, -0.5);
//    glEnd();
//
//
//}
//void drawCubeLight() 
//{
//    //FRONT
//    glBegin(GL_POLYGON);
//    glColor3f(1.0, 1.0, 1.0); 
//    glVertex3f(0.5, -0.5, -0.5);
//    glVertex3f(0.5, 0.5, -0.5);
//    //glColor3f(0.6, 0.0, 0.0);
//    glVertex3f(-0.5, 0.5, -0.5);
//    glVertex3f(-0.5, -0.5, -0.5);
//    glEnd();
//
//    //BACK
//    glBegin(GL_POLYGON);
//    glColor3f(0.3, 0.3, 0.3);
//    glVertex3f(0.5, -0.5, 0.5);
//    glVertex3f(0.5, 0.5, 0.5);
//    glVertex3f(-0.5, 0.5, 0.5);
//    glVertex3f(-0.5, -0.5, 0.5);
//    glEnd();
//
//    //RIGHT
//    glBegin(GL_POLYGON);
//    glColor3f(0.3, 0.3, 0.3);
//    glVertex3f(0.5, -0.5, -0.5);
//    glVertex3f(0.5, 0.5, -0.5);
//    glVertex3f(0.5, 0.5, 0.5);
//    glVertex3f(0.5, -0.5, 0.5);
//    glEnd();
//
//    //LEFT
//    glBegin(GL_POLYGON); 
//    glColor3f(0.3, 0.3, 0.3);
//    glVertex3f(-0.5, -0.5, 0.5);
//    glVertex3f(-0.5, 0.5, 0.5);
//    glVertex3f(-0.5, 0.5, -0.5);
//    glVertex3f(-0.5, -0.5, -0.5);
//    glEnd();
//
//    //TOP
//    glBegin(GL_POLYGON);
//    glColor3f(0.3, 0.3, 0.3);
//    glVertex3f(0.5, 0.5, 0.5);
//    glVertex3f(0.5, 0.5, -0.5);
//    glVertex3f(-0.5, 0.5, -0.5);
//    glVertex3f(-0.5, 0.5, 0.5);
//    glEnd();
//
//    //BOTTOM
//    glBegin(GL_POLYGON);
//    glColor3f(0.3, 0.3, 0.3);
//    glVertex3f(0.5, -0.5, -0.5);
//    glVertex3f(0.5, -0.5, 0.5);
//    glVertex3f(-0.5, -0.5, 0.5);
//    glVertex3f(-0.5, -0.5, -0.5);
//    glEnd();
//
//}
//void _drawCube()
//{
//    /*glVertexPointer(2, GL_FLOAT, 0, cubeArr);
//    glEnableClientState(GL_VERTEX_ARRAY);
//    glColor3f(0.1f, 1.0f, 0.4f);
//    glDrawArrays(GL_POLYGON, 0, 8);
//    glDisableClientState(GL_VERTEX_ARRAY);
//
//    glVertexPointer(2, GL_FLOAT, 0, cubeArr);
//    glEnableClientState(GL_VERTEX_ARRAY);
//    glColor3f(1.0f, 1.0f, 0.4f);
//    glDrawArrays(GL_POLYGON, 8, 17);
//    glDisableClientState(GL_VERTEX_ARRAY);*/
//
//    glVertexPointer(3, GL_FLOAT, 0, cubeArr);
//    glEnableClientState(GL_VERTEX_ARRAY);
//    glColor3f(0.6f, 0.0f, 2.0f);
//    glDrawArrays(GL_POLYGON, 0, 8);
//    glColor3f(0.6f, 2.0f, 0.0f);
//    glDrawArrays(GL_POLYGON, 12, 23);
//    glDisableClientState(GL_VERTEX_ARRAY);
//}
void edge()
{
    float norm[3] = { 1. , 1. , 1. };
    glBegin(GL_TRIANGLES);

    point1[0] = pointX + space;
    point1[1] = space;
    point1[2] = space;

    point2[0] = space;
    point2[1] = pointY + space;
    point2[2] = space;

    point3[0] = space;
    point3[1] = space;
    point3[2] = pointZ + space;

    //getNormal(point1, point2, point3, norm);
    glNormal3fv(norm);


        glVertex3fv(point1);
        glVertex3fv(point2);
        glVertex3fv(point3);
    glEnd();
}
void drawWhiteCube()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    //FRONT
    glBegin(GL_POLYGON);
    glNormal3f(0.0, 0.0, 1.0);
    glVertex3f(0.5, -0.5, -0.5);
    glNormal3f(0.0, 0.0, 1.0);
    glVertex3f(0.5, 0.5, -0.5);
    glNormal3f(0.0, 0.0, 1.0);
    glVertex3f(-0.5, 0.5, -0.5);
    glNormal3f(0.0, 0.0, 1.0);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();

    //BACK
    glBegin(GL_POLYGON);
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(0.5, -0.5, 0.5);
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(0.5, 0.5, 0.5);
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(-0.5, 0.5, 0.5);
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(-0.5, -0.5, 0.5);
    glEnd();

    //RIGHT
    glBegin(GL_POLYGON);
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(0.5, -0.5, -0.5);
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(0.5, 0.5, -0.5);
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(0.5, 0.5, 0.5);
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(0.5, -0.5, 0.5);
    glEnd();

    //LEFT
    glBegin(GL_POLYGON);
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-0.5, -0.5, 0.5);
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-0.5, 0.5, 0.5);
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-0.5, 0.5, -0.5);
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();

    //TOP
    glBegin(GL_POLYGON);
    glNormal3f(0.0, 1.0, 0.0);
    glVertex3f(0.5, 0.5, 0.5);
    glNormal3f(0.0, 1.0, 0.0);
    glVertex3f(0.5, 0.5, -0.5);
    glNormal3f(0.0, 1.0, 0.0);
    glVertex3f(-0.5, 0.5, -0.5);
    glNormal3f(0.0, 1.0, 0.0);
    glVertex3f(-0.5, 0.5, 0.5);
    glEnd();

    //BOTTOM
    glBegin(GL_POLYGON);
    glNormal3f(0.0, -1.0, 0.0);
    glVertex3f(0.5, -0.5, -0.5);
    glNormal3f(0.0, -1.0, 0.0);
    glVertex3f(0.5, -0.5, 0.5);
    glNormal3f(0.0, -1.0, 0.0);
    glVertex3f(-0.5, -0.5, 0.5);
    glNormal3f(0.0, -1.0, 0.0);
    glVertex3f(-0.5, -0.5, -0.5);
    glEnd();
}

void movement_logic()
{
    switch(cube_rot)
    {
		case cube_y_right_rotation:
			rotate_y += speed_of_cube_rotation;
			break;
		case cube_y_left_rotation:
			rotate_y -= speed_of_cube_rotation;
			break;
		case cube_x_up_rotation:
			rotate_x += speed_of_cube_rotation;
			break;
		case cube_x_down_rotation:
			rotate_x -= speed_of_cube_rotation;
			break;
		
		default:
			break;
	}
    switch (sphere_rot)
	{
	case sphere_left_rotation:
		if(sphereLightnessZ >=0)
			sphereLightnessZ -= speed_of_sphere_rotation;
		else sphereLightnessZ = 360.0f;
		break;
	case sphere_right_rotation:
		if(sphereLightnessZ >=0)
			sphereLightnessZ += speed_of_sphere_rotation;
		else sphereLightnessZ = 360.0f;
	default:
		break;
	}
    glutPostRedisplay();
}

void drawSphere(){
	glColor3f(1.0,1.0,1.0);
	glTranslatef(0.0,0.0,0.24);
	glutWireSphere(0.5,0.5,0.5);
}

void drawPlate()
{
    float normal[3] = { 1.0f, 1.0f, 1.0 };
    glBegin(GL_POLYGON);
    point1[0] = pointX + dir;
    point1[1] = pointY + dir;
    point1[2] = pointZ + dir;

    point2[0] = pointX + dir;
    point2[1] = pointY + dir;
    point2[2] = pointZ + dir;

    point3[0] = pointX + dir;
    point3[1] = pointY + dir;
    point3[2] = pointZ + dir;

    point4[0] = pointX + dir;
    point4[0] = pointY + dir;
    point4[0] = pointZ + dir;

    glColor3f(1.0, 0.0, 0.0);
    glVertex3fv(point1);
    glVertex3fv(point2);
    glVertex3fv(point3);
    glVertex3fv(point4);
    //glVertex3f(point1[0], point1[1], point1[2]);
    //glVertex3f(point2[0], point2[1], point2[2]);
    //glVertex3f(point3[0], point3[1], point3[2]);
    //glVertex3f(point4[0], point4[1], point4[2]);
    glEnd();
}
/*
void Light()
{
    glColor4f(1.0f, 1.0f, 1.0f, alpha);
    //glTranslatef(0.0f, 0.0f, 0.0f);

    glScalef(0.1, 0.1, 0.1);
    drawWhiteCube();
    //float lightPosition[] = { 0.0, 0.0, 190, 0.0 };
    //glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    
}
*/
void _display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    if (light) glEnable(GL_LIGHT0);
}
void display() {
    // ������ � ����� ������ ��������� � ����
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    /*
    if (isAlpha) 
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    }
    else 
    {
        glDisable(GL_BLEND);
    }
    */
    if(blend)
    {
        glEnable(GL_BLEND);
        glDepthMask(GL_FALSE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    }
    if(cube_is_multicolor)
    {
        glPushMatrix();
        glRotatef(rotate_x, 1.0, 0.0, 0.0);
        glRotatef(rotate_y, 0.0, 1.0, 0.0);
        drawCube();
        movement_logic();
        glPopMatrix();
    }
    else 
	{
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();
		glRotatef(rotate_x, 1.0, 0.0, 0.0);
		glRotatef(rotate_y, 0.0, 1.0, 0.0);
		drawCube();
        movement_logic();
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
		
	}
	if(blend)
    {
		glDepthMask(GL_TRUE);
		glDisable(GL_BLEND);
	}
    /*
    glPushMatrix();
    glRotatef(rotate_x, 1.0, 0.0, 0.0);
    glRotatef(rotate_y, 0.0, 1.0, 0.0);
    drawCube();
    glPopMatrix();
    */

    glPushMatrix();
	glRotatef(sphereLightnessZ,0.0,0.5,0.0);
	drawSphere();
	GLfloat sphere_lightness[] = {0.0,0.0,0.5,0.0};
	glLightfv(GL_LIGHT0, GL_POSITION, sphere_lightness);
	glPopMatrix();

    theta += 0.01f;
    //std::cout << theta << std::endl;
    //Light();
    glDisable(GL_LIGHTING);
    //glRotatef(angleOfRotation, 1., 0., 0)

    glutSwapBuffers();

    if(delta > 0.4f) delta = 0.4f;
    if(delta < 0.0f) delta = 0.0f;
    if (light == true) glEnable(GL_LIGHTING);
    else glDisable(GL_LIGHTING);
}

void specialKeys(int key, int x, int y) {

    switch (key)
    {
    default:
        break;

    case GLUT_KEY_RIGHT:

			if(cube_rot == non ){
				cube_rot = cube_y_right_rotation;
			}
			else cube_rot = non;
			break;
		case GLUT_KEY_LEFT:
	
			if(cube_rot == non ){
				cube_rot = cube_y_left_rotation;
			}
			else cube_rot = non;
			
			break;
		case GLUT_KEY_UP:
			if(cube_rot == non ){
				cube_rot = cube_x_up_rotation;
			}
			else cube_rot = non;
			
			break;
		case GLUT_KEY_DOWN:
			if(cube_rot == non ){
				cube_rot = cube_x_down_rotation;
			}
			else cube_rot = non;
			
			break;

    case GLUT_KEY_PAGE_UP:
			if(sphere_rot == nope)
				sphere_rot = sphere_left_rotation;
			else sphere_rot = nope;
			
			break;
		case GLUT_KEY_PAGE_DOWN:
			if(sphere_rot == nope)
				sphere_rot = sphere_right_rotation;
			else sphere_rot = nope;
			break;


    case GLUT_KEY_F1:
        light = !light;
        break;

    case GLUT_KEY_F2:
        if (light == true) std::cout << "Light ALL\n";
        if (light == false) std::cout << "Light\n";
        break;

    case GLUT_KEY_F3:
        delta += 0.01f;
        glutPostRedisplay();
        break;
    
    case GLUT_KEY_F4:
        delta -= 0.01f;
        glutPostRedisplay();
        break;

    case GLUT_KEY_F5:
			cube_is_multicolor = true;
			cube_is_multitexture = false;
			cube_is_textured = false;
			break;

    case GLUT_KEY_F7:
			
			cube_is_multicolor = false;
			cube_is_multitexture = true;
			//glEnable(GL_TEXTURE_2D);
			cube_is_textured = true;
			//load_texture();
			break;
		
    case GLUT_KEY_F8:
        if (isAlpha) isAlpha = false;
        else isAlpha = true;
        if (alpha == 1.0) {
            alpha = 0.5;
        }
        else {
            alpha = 1.0;
        }
        if (alpha < 1) {
            glDepthFunc(GL_ALWAYS); 
        }
        else {
            glDepthFunc(GL_LESS);
        }
        break;

    case GLUT_KEY_F9:
        autoRotate = !autoRotate;
        glutPostRedisplay();
        break;

    }
}

void init()
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    GLfloat outside_light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, outside_light_diffuse);
    //glHint(GL_MUL)
    glEnable(GLUT_MULTISAMPLE);
    //glDepthFunc(GL_LESS);

}

//void load_textures()
//{
//    text_img* texture = new text_img[6];
//
//    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
//    glGenTextures(6, textures);
//
//    for (int i = 0, texture_pos = 0; i < 6; i++)
//    {
//        texture[i].data = stbi_load(text_name[texture_pos], &texture[i].sizeX, &texture[i].sizeY, &texture[i].n, STBI_rgb);
//        if (texture[i].data == nullptr) { std::cout << "No load texture\n"; exit(0); }
//        
//        glBindTexture(GL_TEXTURE_2D, textures[i]);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//        glTexImage2D(GL_TEXTURE_2D, 0, 3, texture[i].sizeX, texture[i].sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, texture[i].data);
//    }
//    delete[] texture;
//}
//GLuint load_texture(const char* filename)
//{
//    GLuint texture;
//    int width, height;
//    unsigned char* data;
//
//    FILE* file;
//    file = fopen(filename, "rb");
//
//    if (file == NULL) return 0;
//    width = 1024;
//    height = 512;
//    data = (unsigned char*)malloc(width * height * 3);
//    //int size = fseek(file,);
//    fread(data, width * height * 3, 1, file);
//    fclose(file);
//
//    for (int i = 0; i < width * height; ++i)
//    {
//        int index = i * 3;
//        unsigned char B, R;
//        B = data[index];
//        R = data[index + 2];
//
//        data[index] = R;
//        data[index + 2] = B;
//    }
//
//    glGenTextures(1, &texture);
//    glBindTexture(GL_TEXTURE_2D, texture);
//    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
//
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
//    free(data);
//
//    return texture;
//}

void reshape(int Width, int Height)
{
    if (Height == 0) Height = 1;

    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(-ScreenW / 2, ScreenW / 2, -ScreenH / 2, ScreenH / 2, -800, 800);
    glMatrixMode(GL_MODELVIEW);


    display();
}

void timer(int t)
{
    //if (autoRotate) rotate_y += dir;

    glutPostRedisplay();
    glutTimerFunc(10, timer, 1);

}

int main(int argc, char* argv[]) 
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowPosition(300, 100);
    glutInitWindowSize(ScreenW, ScreenH);
    glutCreateWindow("OpenGL");
    
    init();
    glutDisplayFunc(display);
    glutTimerFunc(10, timer, 1);
    glutReshapeFunc(reshape);
    glutSpecialFunc(specialKeys);

    glutMainLoop();
    return 0;

}

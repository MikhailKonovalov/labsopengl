#include <stdio.h>
#include <GL/glut.h>
#include <math.h>


bool night = false;

GLint background[] = {
    0,   0,
    800, 0,
    800, 300,
    0,   300,
    0,   600,
    800, 600
};

GLint house[] = {
    500, 180,
    700, 180,
    700, 340,
    600, 450,
    500, 340
};

GLint window[] = {
    550, 220,
    650, 220,
    650, 320,
    550, 320
};

void drawCircle(float cx, float cy, float r, int num_segments, float red, float green, float blue, bool fullCircle) // ��� ������
{
    if (fullCircle == true)
    {
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(red, green, blue);
        for (int i = 0; i < num_segments; i++)
        {
            float th = 2.0f * 3.1415926f * float(i) / float(num_segments);

            float x = r * cosf(th);
            float y = r * sinf(th);
            glVertex2f(x + cx, y + cy);
        }
        glEnd();
    }
    else
    {
        
        glBegin(GL_LINE_LOOP);
        glColor3f(red, green, blue);
        for (int i = 0; i < num_segments; i++)
        {
            float th = 2.0f * 3.1415926f * float(i) / float(num_segments);

            float x = r * cosf(th);
            float y = r * sinf(th);

            glVertex2f(x + cx, y + cy);
        }
        glEnd();
    }


}
void drawBackground(bool nightBool)
{
    if (nightBool == true) 
    {
        glVertexPointer(2, GL_INT, 0, background);
        glEnableClientState(GL_VERTEX_ARRAY);
        glColor3f(0.1f, 1.0f, 0.4f);
        glDrawArrays(GL_QUADS, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);

        glVertexPointer(2, GL_INT, 0, background);
        glEnableClientState(GL_VERTEX_ARRAY);
        glColor3f(0.1f, 0.3f, 1.0f);
        glDrawArrays(GL_QUADS, 2, 4);
        glDisableClientState(GL_VERTEX_ARRAY);
    }
    
    else
    {
        glVertexPointer(2, GL_INT, 0, background);
        glEnableClientState(GL_VERTEX_ARRAY);
        glColor3f(0.1f, 1.0f, 0.4f);
        glDrawArrays(GL_QUADS, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);

        glVertexPointer(2, GL_INT, 0, background);
        glEnableClientState(GL_VERTEX_ARRAY);
        glColor3f(0.1f, 0.8f, 1.0f);
        glDrawArrays(GL_QUADS, 2, 4);
        glDisableClientState(GL_VERTEX_ARRAY);
    }
}
void drawWindow() {
    // ������ ���
    glVertexPointer(2, GL_INT, 0, window);
    glEnableClientState(GL_VERTEX_ARRAY);
    glColor3f(1.0f, 1.0f, 1.0f);
    glDrawArrays(GL_POLYGON, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);
    // ���������
    glVertexPointer(2, GL_INT, 0, window);
    glEnableClientState(GL_VERTEX_ARRAY);
    glColor3f(0.0f, 0.0f, 0.0f);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);
    // ����� ����
    glBegin(GL_LINE_LOOP);
    glColor3f(0.0f, 0.0f, 0.0f);
    glVertex2i(550, 270);
    glVertex2i(650, 270);
    glEnd();
    glBegin(GL_LINE_LOOP);
    glColor3f(0.0f, 0.0f, 0.0f);
    glVertex2i(600, 220);
    glVertex2i(600, 320);
    glEnd();

}
void drawHouse()
{
    glVertexPointer(2, GL_INT, 0, house);
    glEnableClientState(GL_VERTEX_ARRAY);
    glColor3f(1.0f, 0.2f, 0.3f);
    glDrawArrays(GL_POLYGON, 0, 5);
    glDisableClientState(GL_VERTEX_ARRAY);
    drawWindow();
    
}

int rx;
int ry;
void display()
{
    static int timeX = 1;
    static int timeY = 500;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    printf("%d\n", timeX);
    
    
    if (night == false)
    {
        drawBackground(false);
        drawHouse();
        drawCircle(timeX/6, timeY ,30, 30, 1.0f, 1.0f, 0.0f, true); // ������
    }
    if (night == true)
    {
        drawBackground(true);
        drawHouse();
        drawCircle((timeX - 5500) / 6 , timeY, 30, 30, 1.0f, 1.0f, 1.0f, true); // ����
    }
    
    timeX++;

    if (timeX > 5500) night = true;
    if (timeX > 11000)
    {
        night = false;
        timeX = 0;
    }


    glutSwapBuffers();
}

void Reshape(int Width, int Height)
{
    if (Height == 0) Height = 1;
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (GLdouble)Width, 0.0, (GLdouble)Height);
    display();
    glutSwapBuffers();
}

void init()
{
    glClearColor(1.0, 1.0, 1.0, 1.0);
  /*  glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(5.0, -5.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);  */ 
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowPosition(400, 400);
    glutInitWindowSize(800, 600);
    glutCreateWindow("OpenGL Window");
    init();
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(Reshape);

    glutMainLoop();
    return 0;
}

#include <stdio.h>
#include <GL/glut.h>
#include <math.h>

bool night = false;

void drawBackground()
{
    glBegin(GL_QUADS);

    glColor3f(0.0, 0.5, 1.0);
    glVertex2i(0, 300);
    glVertex2i(800, 300);
    glColor3f(0.0, 0.5, 0.6);
    glVertex2i(800, 600);
    glVertex2i(0, 600);

    glColor3f(0.0, 0.5, 0.0); // 0.0 0.5 0.0
    glVertex2i(0, 0);
    glVertex2i(800, 0);
    glColor3f(0.0, 0.3, 0.0); // 0.0 0.3 0.0
    glVertex2i(800, 300);
    glVertex2i(0, 300);

    glEnd();
}

void drawNightBackground()
{
    glBegin(GL_QUADS);

    glColor3f(0.0, 0.2, 1.0);
    glVertex2i(0, 300);
    glVertex2i(800, 300);
    glColor3f(0.0, 0.5, 1.3);
    glVertex2i(800, 600);
    glVertex2i(0, 600);

    glColor3f(0.0, 0.5, 0.0);
    glVertex2i(0, 0);
    glVertex2i(800, 0);
    glColor3f(0.0, 0.2, 0.0);
    glVertex2i(800, 300);
    glVertex2i(0, 300);

    glEnd();
}

void drawHouse()
{
    glBegin(GL_QUADS);
    glColor3f(0.5, 0.0, 0.0);
    glVertex2i(350, 130);
    glVertex2i(350, 360);
    glVertex2i(550, 360);
    glVertex2i(550, 130);
    glEnd();

    glBegin(GL_LINE_LOOP);
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex2i(380, 160);
    glVertex2i(380, 300);
    glVertex2i(520, 300);
    glVertex2i(520, 160);
    glEnd();
    
    glBegin(GL_LINE_LOOP);
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex2i(460, 160);
    glVertex2i(460, 300);
    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBegin(GL_POLYGON);
    glColor3f(0.8, 0.0, 0.0);
    glVertex2i(350, 360);
    glVertex2i(450, 500);
    glVertex2i(550, 360);
    glEnd();

}

void drawSun(float cx, float cy, float r, int num_segments)
{
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1.0f, 1.0f, 0.0f);
    for (int i = 0; i < num_segments; i++)
    {
        float th = 2.0f * 3.1415926f * float(i) / float(num_segments);

        float x = r * cosf(th);
        float y = r * sinf(th);
        glVertex2f(x + cx , y + cy);
    }
    glEnd();
}

void drawMoon(float cx, float cy, float r, int num_segments)
{
    glBegin(GL_POLYGON_BIT);
    glColor3f(1.0f, 1.0f, 0.0f);
    for (int i = 0; i < num_segments; i++)
    {
        float th = 1.f * 3.1415926f * float(i) / float(num_segments);

        float x = r * sinf(th);
        float y = r * cosf(th);
        glVertex2f(x + cx, y + cy);
    }
    glEnd();
}

void display()
{
    static int time = 1;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    printf("%d\n", time);

    if (night == false) // ����
    {
        drawBackground();
        drawHouse();
        drawSun((time - 50) / 7, 550, 20, 10); // ������
    }
    if (night == true) // ����
    {
        drawNightBackground();
        drawHouse();
        drawMoon((time - 6050) / 7, 550, 20, 10); // ����
    }
    time++;
    if (time > 6000)
    {
        night = true;
    }
    if (time > 12000)
    {
        night = false;
        time = 0;
    }

    glutSwapBuffers();
}

void Reshape(int Width, int Height)
{
    if (Height == 0)
    {
        Height = 1;
    }
    glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (GLdouble)Width, 0.0, (GLdouble)Height);
    display();
    glutSwapBuffers();
}

void init()
{
    glClearColor(1.0, 1.0, 1.0, 1.0);
}

int main(int argc,char** argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowPosition(400,400);
    glutInitWindowSize(800,600);
    glutCreateWindow("OpenGL Window");
    init();
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(Reshape);
    glutMainLoop();
    return 0;
}
